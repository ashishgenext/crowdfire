package com.wardrobeapp.database.table;

/**
 * Created by ashish.am.singh on 13-06-2016.
 */
public class FavouriteTable {
    public static final String TABLE_NAME = "favourite_table";

    public static final String ID ="id";
    public static final String TOP_FILE_NAME = "top_file_name";
    public static final String BOTTOM_FILE_NAME = "bottom_file_name";

    public static final String TABLE_SQL = "CREATE TABLE IF NOT EXISTS "
            + TABLE_NAME + " (" + ID +" INTEGER PRIMARY KEY ,"+ TOP_FILE_NAME + " TEXT ," + BOTTOM_FILE_NAME + " TEXT " + ");";
}
