package com.wardrobeapp.ui.activity;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import com.wardrobeapp.R;
import com.wardrobeapp.adapter.SlidePagerAdapter;
import com.wardrobeapp.database.DbOperations;
import com.wardrobeapp.receiver.NotifyReceiver;
import com.wardrobeapp.ui.animation.PageChanger;
import com.wardrobeapp.utils.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class MainActivity extends FragmentActivity {

    private ViewPager mTopViewPager ,mBottomViewPager;
    private SlidePagerAdapter mTopViewPagerAdapter ,mBottomViewPagerAdapter;
    ArrayList <String> mTopItemArray,mBottomItemArray ;
    private ImageView mTopImage ,mBottomImage;
    private ImageView mFavouriteImage,mLoadImage;
    private static int CAMERA_REQUEST = 0 ;
    private static int GALLERY_REQUEST = 1 ;
    private int mCurrentTopLocation = -1 ,mCurrentBottomLocation = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState != null){
            mCurrentTopLocation =  savedInstanceState.getInt("topImageIndex");
            mCurrentBottomLocation = savedInstanceState.getInt("bottomIndex");
        }


        FileUtils.setAlarm(getApplicationContext());
        mTopItemArray = DbOperations.ImageQuery(1);
        mBottomItemArray = DbOperations.ImageQuery(2);

        mTopViewPager = (ViewPager) findViewById(R.id.shirt_pager);
        mBottomViewPager = (ViewPager) findViewById(R.id.pant_pager);
        mTopViewPagerAdapter = new SlidePagerAdapter(this,mTopItemArray);
        mBottomViewPagerAdapter = new SlidePagerAdapter(this,mBottomItemArray);

        mTopImage = (ImageView) findViewById(R.id.add_shirt);
        mBottomImage = (ImageView) findViewById(R.id.add_pant);
        mTopImage.setOnClickListener(mTopClickListener);
        mBottomImage.setOnClickListener(mBottomClickListener);
        mTopImage.setFocusable(true);
        mBottomImage.setFocusable(true);
        mTopImage.setClickable(true);
        mBottomImage.setClickable(true);

        mTopViewPager.setAdapter(mTopViewPagerAdapter);
        mBottomViewPager.setAdapter(mBottomViewPagerAdapter);
        mTopViewPager.setClickable(false);
        mBottomViewPager.setClickable(false);
        mTopViewPager.setPageTransformer(true, new PageChanger());
        mBottomViewPager.setPageTransformer(true, new PageChanger());
        mTopViewPager.setFocusable(false);
        mBottomViewPager.setFocusable(false);

        mFavouriteImage = (ImageView) findViewById(R.id.favourite);
        mFavouriteImage.setImageResource(R.drawable.bheart_32);
        mFavouriteImage.setTag(R.string.image_tag,1);

        if(mCurrentBottomLocation != -1 && mCurrentTopLocation != -1 ){
            mTopViewPager.setCurrentItem(mCurrentTopLocation);
            mBottomViewPager.setCurrentItem(mCurrentBottomLocation);

        }

        if(mTopItemArray.size() !=0 && mBottomItemArray.size() !=0) {
            String bottomImage = DbOperations.bottomFavouriteImageQuery(mTopItemArray.get(0));
            if (bottomImage.equals(mBottomItemArray.get(0))) {
                mFavouriteImage.setImageResource(R.drawable.rheart_32);
                mFavouriteImage.setTag(R.string.image_tag, 2);
            } else {
                mFavouriteImage.setImageResource(R.drawable.bheart_32);
                mFavouriteImage.setTag(R.string.image_tag, 1);
            }
        }

        mLoadImage = (ImageView) findViewById(R.id.reload);
        mLoadImage.setOnClickListener(mOnRefreshClick);
        mFavouriteImage.setOnClickListener(mOnFavouriteClick);


       if(mCurrentTopLocation != -1 && mCurrentBottomLocation != -1){
           mTopViewPager.setCurrentItem(mCurrentTopLocation);
           mBottomViewPager.setCurrentItem(mCurrentBottomLocation);
       }
       mTopViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
           @Override
           public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

           }

           @Override
           public void onPageSelected(int position) {
                mCurrentTopLocation = position ;
               String bottomImage = DbOperations.bottomFavouriteImageQuery(mTopItemArray.get(position));
               if(mBottomItemArray.size() != 0 && mCurrentBottomLocation != -1) {
                   if (bottomImage.equals(mBottomItemArray.get(mCurrentBottomLocation))) {
                       mFavouriteImage.setImageResource(R.drawable.rheart_32);
                   }else{
                       mFavouriteImage.setImageResource(R.drawable.bheart_32);
                   }
               } else {
                   mFavouriteImage.setImageResource(R.drawable.bheart_32);
               }
           }

           @Override
           public void onPageScrollStateChanged(int state) {

           }
       });

        mBottomViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mCurrentBottomLocation = position ;
                String topImage = DbOperations.topFavouriteImageQuery(mBottomItemArray.get(position));
                if(mTopItemArray.size() !=0 && mCurrentTopLocation != -1){
                if(topImage.equals(mTopItemArray.get(mCurrentTopLocation))){
                    mFavouriteImage.setImageResource(R.drawable.rheart_32);
                }else{
                    mFavouriteImage.setImageResource(R.drawable.bheart_32);
                }
            }else{
                    mFavouriteImage.setImageResource(R.drawable.bheart_32);
            }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });



    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
          if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    View.OnClickListener mOnRefreshClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int sizeA = mTopItemArray.size();
            int sizeB = mBottomItemArray.size();

            if(sizeA > 1 && sizeB >1){
                mTopViewPager.setCurrentItem(FileUtils.generateRandom(0, sizeA - 1));
                mBottomViewPager.setCurrentItem(FileUtils.generateRandom(0, sizeB - 1));
            }

        }
    };



    View.OnClickListener mTopClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

           openDialog(v, 1);
        }
    };

    View.OnClickListener mBottomClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            openDialog(v,2);
        }
    };

    View.OnClickListener mOnFavouriteClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(mBottomItemArray.size() != 0 && mTopItemArray.size() !=0) {
                String img1 = mTopItemArray.get(mTopViewPager.getCurrentItem());
                String img2 = mBottomItemArray.get(mBottomViewPager.getCurrentItem());

                if (v.getTag(R.string.image_tag) == 1) {
                    DbOperations.insertFavImage(img1, img2);
                    mFavouriteImage.setImageResource(R.drawable.rheart_32);
                    v.setTag(R.string.image_tag, 2);
                } else {
                    DbOperations.deleteFavQuery(img1, img2);
                    mFavouriteImage.setImageResource(R.drawable.bheart_32);
                    v.setTag(R.string.image_tag, 1);
                }
            }

        }
    };

    private int mImageType = -1 ;

    public void openDialog(View view,int type){

        mImageType = type ;
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Select Gallery or Camera");

        alertDialogBuilder.setPositiveButton("Camera", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                takePicture.putExtra(MediaStore.EXTRA_OUTPUT, setImageUri());
                startActivityForResult(takePicture, CAMERA_REQUEST);
            }
        });

        alertDialogBuilder.setNegativeButton("Gallery",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , GALLERY_REQUEST);
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch(requestCode) {
            case 0:

                if(resultCode == RESULT_OK ){
                    String selectedImage = getImagePath();
                    DbOperations.insertImage(selectedImage.toString(),mImageType);

                    if(mImageType == 1 ){
                        mTopItemArray.add(selectedImage);
                    }else{
                        mBottomItemArray.add(selectedImage);
                    }
                }


                break;
            case 1:

                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    String imagePath = FileUtils.getRealPathFromURI(this,selectedImage);
                    DbOperations.insertImage(imagePath,mImageType);

                    if(mImageType == 1 ){
                        mTopItemArray.add(imagePath);
                    }else{
                        mBottomItemArray.add(imagePath);
                    }
                }
                break;
        }

        mBottomViewPager.getAdapter().notifyDataSetChanged();
        mTopViewPager.getAdapter().notifyDataSetChanged();
    }

    private String mImagePath = "";

    public Uri setImageUri() {
        File file = new File(Environment.getExternalStorageDirectory() + "/DCIM/", "image" + new Date().getTime() + ".png");
        Uri imgUri = Uri.fromFile(file);
        mImagePath = file.getAbsolutePath();
        return imgUri;
    }

    public String getImagePath() {
        return mImagePath;
    }
    public String getAbsolutePath(Uri uri) {
        String[] projection = { MediaStore.MediaColumns.DATA };
        @SuppressWarnings("deprecation")
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
          //  setContentView(R.layout.activity_main_landscape);
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
           // setContentView(R.layout.activity_main);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {

        outState.putInt("topImageIndex", mCurrentTopLocation);
        outState.putInt("bottomIndex",mCurrentBottomLocation);
        super.onSaveInstanceState(outState, outPersistentState);

    }






}
