package com.wardrobeapp.adapter;

import android.content.Context;

import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.wardrobeapp.R;
import com.wardrobeapp.utils.FileUtils;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by ashish.am.singh on 13-06-2016.
 */
public class SlidePagerAdapter extends PagerAdapter{

    ArrayList<String> mImagePath ;
    LayoutInflater mLayoutInflater;
    Context mContext ;
    public SlidePagerAdapter(Context context,ArrayList<String> imagePath) {
        mImagePath = imagePath ;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = context ;

    }

    @Override
    public int getCount() {
        return mImagePath.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.fragment_slide_page, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.slider_image);
        File imgFile = new  File(mImagePath.get(position));
        if(imgFile.exists()){
            imageView.setImageBitmap(FileUtils.decodeFile(mContext,mImagePath.get(position)));
        }
        container.addView(itemView);
        return itemView;
    }

}
