package com.wardrobeapp.ui.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.wardrobeapp.R;

import java.io.File;

/**
 * Created by ashish.am.singh on 13-06-2016.
 */
public class SliderFragment extends Fragment {

    public static final String ARG_PAGE = "page",ARG_PATH = "path";
    private static Bitmap mBitmap;
    private int mPageNumber;
    private String mImagePath;

    public static SliderFragment create(int pageNumber,String path) {
        Bundle args = new Bundle();
        SliderFragment fragment = new SliderFragment();
        args.putInt(ARG_PAGE, pageNumber);
        args.putString(ARG_PATH,path);
        fragment.setArguments(args);
        return fragment;
    }

    public SliderFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNumber = getArguments().getInt(ARG_PAGE);
        mImagePath = getArguments().getString(ARG_PATH);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_slide_page, container, false);
        ImageView image = (ImageView) rootView.findViewById(R.id.slider_image);

        File imgFile = new  File(mImagePath);

        if(imgFile.exists()){
           // Bitmap myBitmap = BitmapFactory.decodeFile(mImagePath);
            image.setImageBitmap(decodeFile(mImagePath));
           // image.setImageBitmap(myBitmap);
        }
        return rootView;
        }

    public Bitmap decodeFile(String path) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, o);
            final int REQUIRED_SIZE = 70;
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeFile(path, o2);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;

    }
}

