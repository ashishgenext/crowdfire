package com.wardrobeapp.database.table;

/**
 * Created by Ashish.Am.Singh on 14-06-2016.
 */
public class ItemsTable {

    public static final String TABLE_NAME = "item_table";

    public static final String FILE_NAME = "file_name";
    public static final String TYPE = "type";

    public static final String TABLE_SQL = "CREATE TABLE IF NOT EXISTS "
            + TABLE_NAME + " (" + FILE_NAME + " TEXT ," + TYPE + " INTEGER " + ");";
}
