package com.wardrobeapp.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.wardrobeapp.database.table.FavouriteTable;
import com.wardrobeapp.database.table.ItemsTable;

import java.util.ArrayList;

/**
 * Created by Ashish.Am.Singh on 14-06-2016.
 */
public class DbOperations {

    public static void insertImage(String imagePath, int value) {

        DbManager dbManagerInstance = DbManager.getInstance();
        SQLiteDatabase db = dbManagerInstance.openDatabase();

        ContentValues values = new ContentValues();
        values.put(ItemsTable.FILE_NAME,imagePath);
        values.put(ItemsTable.TYPE, value);

        try {
            db.insert(ItemsTable.TABLE_NAME, null, values);
        } finally {
            dbManagerInstance.closeDatabase();
        }
    }

    public static void insertFavImage(String img1, String img2) {

        DbManager dbManagerInstance = DbManager.getInstance();
        SQLiteDatabase db = dbManagerInstance.openDatabase();

        ContentValues values = new ContentValues();
        values.put(FavouriteTable.TOP_FILE_NAME,img1);
        values.put(FavouriteTable.BOTTOM_FILE_NAME, img2);

        try {
            db.insert(FavouriteTable.TABLE_NAME, null, values);
        } finally {
            dbManagerInstance.closeDatabase();
        }
    }

    public static ArrayList<String> ImageQuery(int type) {

        DbManager dbManagerInstance = DbManager.getInstance();
        SQLiteDatabase db = dbManagerInstance.openDatabase();
        String value = "";
        Cursor cr = null;
        ArrayList<String> list = new ArrayList<String>();
        String selectQuery = "SELECT * FROM "+ ItemsTable.TABLE_NAME +" where "+ItemsTable.TYPE+"=?";
        try {
            cr = db.rawQuery(selectQuery, new String[]{String.valueOf(type)});

            while(cr.moveToNext()) {
                list.add(cr.getString(cr.getColumnIndex(ItemsTable.FILE_NAME)));

            }
        }finally {
            if(cr != null){
                cr.close();
            }
            dbManagerInstance.closeDatabase();
        }

        return  list ;
    }

    public static String topFavouriteImageQuery(String imagePath) {

        DbManager dbManagerInstance = DbManager.getInstance();
        SQLiteDatabase db = dbManagerInstance.openDatabase();
        String value = "";
        Cursor cr = null;

        String selectQuery = "SELECT * FROM "+ FavouriteTable.TABLE_NAME+" where "+FavouriteTable.BOTTOM_FILE_NAME+"=?" ;
        try {
            cr = db.rawQuery(selectQuery, new String[]{imagePath});

            while(cr.moveToNext()) {
                value = cr.getString(cr.getColumnIndex(FavouriteTable.TOP_FILE_NAME));

            }
        }finally {
            if(cr != null){
                cr.close();
            }
            dbManagerInstance.closeDatabase();
        }

        return  value ;
    }

    public static String bottomFavouriteImageQuery(String imagePath) {

        DbManager dbManagerInstance = DbManager.getInstance();
        SQLiteDatabase db = dbManagerInstance.openDatabase();
        String value = "";
        Cursor cr = null;
        //  ArrayList<String> list = new ArrayList<String>();
        String selectQuery = "SELECT * FROM "+ FavouriteTable.TABLE_NAME+" where "+FavouriteTable.TOP_FILE_NAME+"=?" ;
        try {
            cr = db.rawQuery(selectQuery, new String[]{imagePath});

            while(cr.moveToNext()) {
                value = cr.getString(cr.getColumnIndex(FavouriteTable.BOTTOM_FILE_NAME));

            }
        }finally {
            if(cr != null){
                cr.close();
            }
            dbManagerInstance.closeDatabase();
        }

        return  value ;
    }

    public static void deleteFavQuery(String topImage,String bottomImage){
        DbManager dbManagerInstance = DbManager.getInstance();
        SQLiteDatabase db = dbManagerInstance.openDatabase();
        String whereClause = FavouriteTable.TOP_FILE_NAME + "=? AND " +FavouriteTable.BOTTOM_FILE_NAME +"=?";
        String[] whereArgs = new String[] { topImage,bottomImage };
        try{
        db.delete(FavouriteTable.TABLE_NAME, whereClause, whereArgs);
        }finally {
            dbManagerInstance.closeDatabase();
        }
        }

}