package com.wardrobeapp.database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.wardrobeapp.database.table.FavouriteTable;
import com.wardrobeapp.database.table.ItemsTable;

/**
 * Created by Ashish on 12-06-2016.
 */
public class DbHelper extends SQLiteOpenHelper {
    private static  final int DB_VERSION = 1;

    public DbHelper(Context context, String name){
        super(context,name,null,DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.beginTransaction();
        try{
            db.execSQL(FavouriteTable.TABLE_SQL);
            db.execSQL(ItemsTable.TABLE_SQL);
            db.setTransactionSuccessful();
        }finally {
            db.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
