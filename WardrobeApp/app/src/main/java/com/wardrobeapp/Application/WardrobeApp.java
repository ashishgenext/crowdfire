package com.wardrobeapp.Application;

import android.app.Application;
import android.os.Environment;
import android.util.Log;

import com.wardrobeapp.database.DbHelper;
import com.wardrobeapp.database.DbManager;

import java.io.File;

/**
 * Created by Ashish.Am.Singh on 14-06-2016.
 */
public class WardrobeApp extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
        DbManager.initializeInstance(new DbHelper(getApplicationContext(),"mydb.db"));
        if(!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
            Log.d("MyApp", "No SDCARD");
        }
        else {
            File directory = new File(Environment.getExternalStorageDirectory()+File.separator+"srcImage");
            directory.mkdirs();
        }
    }
}
